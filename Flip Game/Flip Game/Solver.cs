﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Flip_Game
{
    class Solver
    {
        private Board currBoard, endBoard;
        private List<Tuple<string, int, int>> seenBoards;
        public List<Tuple<int, int>> moveList;
        private Queue<Board> toDo;
        public bool solveable;

        public Solver(ContentManager content)
        {
            endBoard = new Board();
            endBoard.Initialize(content);
            seenBoards = new List<Tuple<string, int, int>>();

            toDo = new Queue<Board>();

            moveList = new List<Tuple<int, int>>();
            solveable = true;
        }

        public bool seen(string id)
        {
            for (int i = 0; i < seenBoards.Count; i++ )
            {
                if (seenBoards[i].Item1 == id)
                    return true;
            }
            return false;
        }

        public void solve(Board problem)
        {
            currBoard = new Board(problem);
            seenBoards = new List<Tuple<string, int, int>>();
            toDo = new Queue<Board>();
            moveList = new List<Tuple<int, int>>();
            toDo.Enqueue(currBoard);


            while (toDo.Count > 0)
            {
                currBoard = toDo.Dequeue();
                if (currBoard.id == endBoard.id)
                {
                    Console.WriteLine("Solved");
                    doit(problem);
                    break;
                }
                else
                {
                    for (int col = 0; col < currBoard.grid.Count; col++)
                    {
                        for (int row = 0; row < currBoard.grid[col].Count; row++)
                        {
                            Board newBoard = new Board(currBoard);
                            newBoard.flip(row, col);
                            if (!seen(newBoard.id))
                            {
                                seenBoards.Add(new Tuple<string, int, int>(newBoard.id, row, col));
                                toDo.Enqueue(newBoard);
                            }

                        }
                    }
                }
            }
            if(toDo.Count == 0)
                solveable = false;
        }

        public void doit(Board board)
        {
            while (currBoard.id != board.id)
            {
                for (int i = 0; i < seenBoards.Count; i++)
                {
                    if (seenBoards[i].Item1 == currBoard.id)
                    {
                        moveList.Add(new Tuple<int, int>(seenBoards[i].Item2, seenBoards[i].Item3));
                        currBoard.flip(seenBoards[i].Item2, seenBoards[i].Item3);
                        break;
                    }
                }
            }

            //for (int i = moveList.Count - 1; i >= 0; i--)
            //{
            //    board.flip(moveList[i].Item1, moveList[i].Item2);
            //}
        }

    }
}
