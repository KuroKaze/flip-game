﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;


namespace Flip_Game
{
    class Board
    {
        public List<List<Entity>> grid;
        public string id;
        Texture2D bg;
        public bool initialInput;

        public Board()
        {
            grid = new List<List<Entity>>();
            initialInput = true;
        }

        public Board(Board board)
        {
            grid = new List<List<Entity>>();
            for (int i = 0; i < 4; i++)
            {
                List<Entity> subList = new List<Entity>();
                for (int j = 0; j < 4; j++)
                {
                    subList.Add(new Coin(board.grid[i][j]));
                }
                grid.Add(subList);
            }
            recomputID();
        }

        public void Initialize(ContentManager content)
        {
            for (int i = 0; i < 4; i++)
            {
                List<Entity> subList = new List<Entity>();
                for (int j = 0; j < 4; j++)
                {
                    subList.Add(new Coin(content.Load<Texture2D>("Assets\\Coin"), 1, 2, 0, 2, new Vector2(i * 60.0f + 5.0f, j * 60.0f + 5.0f)));
                }
                grid.Add(subList);
            }
            bg = content.Load<Texture2D>("Assets\\Board");
            recomputID();
        }

        //public void randInitialize(ContentManager content)
        //{
        //    for (int i = 0; i < 4; i++)
        //    {
        //        List<Entity> subList = new List<Entity>();
        //        for (int j = 0; j < 4; j++)
        //            subList.Add(new Coin(content.Load<Texture2D>("Assets\\Coin"), 1, 2, 0, 2, new Vector2(i * 60.0f + 5.0f, j * 60.0f + 5.0f)));
        //        grid.Add(subList);
        //    }
        //    Random rand = new Random();
        //    for (int col = 0; col < grid.Count; col++)
        //    {
        //        for (int row = 0; row < grid[col].Count; row++)
        //        {
        //            if (rand.Next(2) == 0)
        //                grid[col][row].Update();
        //        }
        //    }
        //    bg = content.Load<Texture2D>("Assets\\Board");
        //    recomputID();
        //}

        public void Update(Input mouseKey)
        {
            if (initialInput || id != "0000000000000000")
            {
                for (int col = 0; col < grid.Count; col++)
                {
                    for (int row = 0; row < grid[col].Count; row++)
                    {
                        if (mouseKey.lMouseBottonClickedOn(grid[col][row]))
                        {
                            if (initialInput)
                            {
                                grid[col][row].Update();
                                recomputID();
                            }
                            else
                                flip(row, col);
                        }
                    }
                }
            }
            mouseKey.Update();
        }

        public void flip(int row, int col)
        {
            grid[col][row].Update();
            if (col >= 0 && col < grid.Count - 1)
                grid[col + 1][row].Update();
            if (col <= grid.Count - 1 && col > 0)
                grid[col - 1][row].Update();
            if (row >= 0 && row < grid[col].Count - 1)
                grid[col][row + 1].Update();
            if (row <= grid[col].Count - 1 && row > 0)
                grid[col][row - 1].Update();

            recomputID();
        }

        public void recomputID()
        {
            id = "";
            foreach (List<Entity> list in grid)
            {
                foreach(Entity ent in list)
                {
                    id += ent.currentFrame;
                }
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(bg, Vector2.Zero, Color.White);
            foreach (List<Entity> col in grid)
            {
                foreach (Entity ent in col)
                {
                    ent.Draw(spriteBatch);
                }
            }
        }
    }
}
