﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


abstract class Entity
{
    private Texture2D spriteMap;
    private Vector2 location;
    public Vector2 Location
    {
        get
        {
            return location;
        }
        set
        {
            location = value;
            if (fromTopLeft)
            {
                boundingBox.X = (int)location.X;
                boundingBox.Y = (int)location.Y;
            }
            else
            {
                boundingBox.X = (int)(location.X - (float)width / 2.0f);
                boundingBox.Y = (int)(location.Y - (float)height / 2.0f);
            }
        }
    }
    public int firstFrame { get; set; }
    public int lastFrames { get; set; }
    public int currentFrame { get; set; }
    public bool fromTopLeft;
    public int width, height, rows, columns;
    public Rectangle boundingBox;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="texture">Sprite map</param>
    /// <param name="numRows/numColumns">Number of rows and columns of sprites on sprite map (one set of frames for animation per row)</param>
    /// <param name="startFrame/endFrame">Frame no. for starting and ending animation</param>
    /// <param name="loc">Location of object</param>
    /// <param name="ftl">If location is measured from top left corner</param>
    public Entity(Texture2D texture, int numRows, int numColumns, int startFrame, int endFrame, Vector2 loc, bool ftl = true)
    {
        spriteMap = texture;
        rows = numRows;
        columns = numColumns;
        location = loc;
        fromTopLeft = ftl;
        firstFrame = startFrame;
        currentFrame = startFrame;
        lastFrames = endFrame;
        width = spriteMap.Width / columns;
        height = spriteMap.Height / rows;
        if (fromTopLeft)
            boundingBox = new Rectangle((int)location.X, (int)location.Y, width, height);
        else
            boundingBox = new Rectangle((int)(location.X - (float)width / 2.0f), (int)(location.Y - (float)height / 2.0f), width, height);
    }

    public Entity(Entity ent)
    {
        spriteMap = ent.spriteMap;
        rows = ent.rows;
        columns = ent.columns;
        location = ent.Location;
        fromTopLeft = ent.fromTopLeft;
        firstFrame = ent.firstFrame;
        currentFrame = ent.currentFrame;
        lastFrames = ent.lastFrames;
        width = spriteMap.Width / columns;
        height = spriteMap.Height / rows;
        if (fromTopLeft)
            boundingBox = new Rectangle((int)location.X, (int)location.Y, width, height);
        else
            boundingBox = new Rectangle((int)(location.X - (float)width / 2.0f), (int)(location.Y - (float)height / 2.0f), width, height);

    }

    public virtual void doWhenCollide()
    {

    }

    public virtual void doWhenClicked()
    {

    }

    /// <summary>
    /// Loops through animations
    /// </summary>
    public virtual void Update()
    {
        currentFrame++;
        if (currentFrame == lastFrames)
            currentFrame = firstFrame;
    }

    /// <summary>
    /// Change animations
    /// </summary>
    /// <param name="startFrame">Frame no. for start of animation</param>
    /// <param name="endFrame">Frame no. for end of animation</param>
    public virtual void Update(int startFrame, int endFrame)
    {
        firstFrame = startFrame;
        currentFrame = firstFrame;
        lastFrames = endFrame;
    }

    /// <summary>
    /// Draws the object
    /// </summary>
    /// <param name="spriteBatch">Enables group of sprites to be drawn</param>
    public virtual void Draw(SpriteBatch spriteBatch)
    {
        int row = currentFrame / columns;
        int column = currentFrame % columns;

        Rectangle srcRectangle = new Rectangle((width + 1) * column, (height + 1) * row, width, height);

        spriteBatch.Draw(spriteMap, boundingBox, srcRectangle, Color.White);
    }
}
