﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

class Input
{
    private KeyboardState oldKeyState;
    private MouseState oldMouseState;
    private Texture2D cursorSpriteMap;
    private const int unClickFrame = 1, clickFrame = 2, loadFrame = 3;
    private int curCursorFrame;

    public Input(Game game, Texture2D texture = null)
    {
        cursorSpriteMap = texture;
        if (texture == null)
            game.IsMouseVisible = true;
        else
            game.IsMouseVisible = false;
        curCursorFrame = 1;
    }

    public bool lMouseButtonClicked()
    {
        MouseState newMouseState = Mouse.GetState();
        return newMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released;
    }

    public bool lMouseBottonClickedOn(Entity ent)
    {
        MouseState newMouseState = Mouse.GetState();
        bool sameLocation = newMouseState.X == oldMouseState.X && newMouseState.Y == oldMouseState.Y;
        bool clicked = newMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released;
        if (clicked && sameLocation)
        {
            return newMouseState.X >= ent.boundingBox.X &&
                   newMouseState.X <= ent.boundingBox.X + ent.boundingBox.Width &&
                   newMouseState.Y >= ent.boundingBox.Y &&
                   newMouseState.Y <= ent.boundingBox.Y + ent.boundingBox.Height;
        }
        else
            return false;
    }

    public bool rMouseButtonClicked()
    {
        MouseState newMouseState = Mouse.GetState();
        return newMouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released;
    }

    public bool rMouseBottonClickedOn(Entity ent)
    {
        MouseState newMouseState = Mouse.GetState();
        bool sameLocation = newMouseState.X == oldMouseState.X && newMouseState.Y == oldMouseState.Y;
        bool clicked = newMouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released;
        if (clicked && sameLocation)
        {
            return newMouseState.X >= ent.boundingBox.X &&
                   newMouseState.X <= ent.boundingBox.X + ent.boundingBox.Width &&
                   newMouseState.Y >= ent.boundingBox.Y &&
                   newMouseState.Y <= ent.boundingBox.Y + ent.boundingBox.Height;
        }
        else
            return false;
    }

    public bool keyPressed(Keys key)
    {
        KeyboardState newKeyState = Keyboard.GetState();
        return newKeyState.IsKeyUp(key) && oldKeyState.IsKeyDown(key);
    }

    public void Update()
    {
        MouseState newMouseState = Mouse.GetState();
        KeyboardState newKeyState = Keyboard.GetState();
        if (cursorSpriteMap != null && newMouseState.LeftButton == ButtonState.Pressed)
            curCursorFrame = 2;
        else if (cursorSpriteMap != null && newMouseState.LeftButton == ButtonState.Released)
            curCursorFrame = 1;
        oldMouseState = newMouseState;
        oldKeyState = newKeyState;
    }

    public void Draw(SpriteBatch spriteBatch)
    {
        if (cursorSpriteMap != null)
        {
            int row = curCursorFrame / 3;
            int column = curCursorFrame % 3;
            int width = cursorSpriteMap.Width / 3;
            int height = cursorSpriteMap.Height / 3;
            Rectangle srcRectangle = new Rectangle((width + 1) * column, (height + 1) * row, width, height);
            spriteBatch.Draw(cursorSpriteMap, new Vector2((float)Mouse.GetState().X, (float)Mouse.GetState().Y), srcRectangle, Color.White);
        }
    }
}