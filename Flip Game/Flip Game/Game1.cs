using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Flip_Game
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Input mouseKey;
        Board board;
        Solver sol;
        SpriteFont arial;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            mouseKey = new Input(this);
            board = new Board();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            board.Initialize(Content);
            sol = new Solver(Content);
            arial = Content.Load<SpriteFont>("Fonts\\Arial");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (mouseKey.keyPressed(Keys.Escape))
                this.Exit();
            if (mouseKey.keyPressed(Keys.S) && !board.initialInput && board.id != "0000000000000000")
                sol.solve(board);
            if (mouseKey.keyPressed(Keys.R))
            {
                board = new Board();
                board.Initialize(Content);
                sol = new Solver(Content);
                sol.solveable = true;
            }
            if (mouseKey.keyPressed(Keys.D))
                board.initialInput = false;
            //mouseKey.Update();
            board.Update(mouseKey);
           
            //Console.WriteLine(
            //boards.Exists(
            //    delegate(Board bord)
            //    {
            //        for (int col = 0; col < board.grid.Count; col++)
            //        {
            //            for (int row = 0; row < board.grid[col].Count; row++ )
            //            {
            //                if (bord.grid[col][row].currentFrame != board.grid[col][row].currentFrame)
            //                    return false;
            //            }
            //        }
            //        return true;
            //    }));
            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Gray);

            spriteBatch.Begin();
            board.Draw(spriteBatch);

            mouseKey.Draw(spriteBatch);

            if (!sol.solveable)
                spriteBatch.DrawString(arial, "Not Solvable", new Vector2(300.0f, 0.0f), Color.White);
            else
                spriteBatch.DrawString(arial, "(Row, Col)", new Vector2(300.0f, 0.0f), Color.White);
            for (int i = sol.moveList.Count - 1; i >= 0; i--)
                spriteBatch.DrawString(arial, "(" + sol.moveList[i].Item1 + ", " + sol.moveList[i].Item2 + ")", new Vector2(300.0f, i * 25.0f + 25.0f), Color.White);

            spriteBatch.DrawString(arial, "r = reset, s = solve, d = done intial input", new Vector2(0.0f, graphics.PreferredBackBufferHeight - 25.0f), Color.White);
            spriteBatch.DrawString(arial, "Initial Input: " + board.initialInput, new Vector2(500.0f, 0.0f), Color.White);

            if (!board.initialInput && board.id == "0000000000000000")
            {
                GraphicsDevice.Clear(Color.Black);
                spriteBatch.DrawString(arial, "WIN!!!", new Vector2(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight/ 2), Color.White);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
