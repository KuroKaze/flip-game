﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flip_Game
{
    class Coin : Entity
    {
        public Coin(Texture2D texture, int numRows, int numColumns, int startFrame, int endFrame, Vector2 loc, bool ftl = true)
            : base(texture, numRows, numColumns, startFrame, endFrame, loc, ftl)
        {
            
        }
        public Coin(Entity coin)
            : base(coin)
        {

        }
    }
}
